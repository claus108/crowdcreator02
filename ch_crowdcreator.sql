-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 10, 2022 at 10:21 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ch_crowdcreator`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `adminID` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`adminID`, `username`, `password`) VALUES
(1, 'clhen106', '$2y$10$W25CuB7eypJbnywiTfC8LOAv9LO517VcQfDG4aNk42VtK8IqestXO');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `eventID` int(11) NOT NULL,
  `organiserID` int(11) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `venue` varchar(50) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `time` varchar(50) DEFAULT NULL,
  `approved` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`eventID`, `organiserID`, `title`, `description`, `venue`, `address`, `date`, `time`, `approved`) VALUES
(1, 7, 'Fisher, Raynor and Schoen', 'Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', 'Kuphal, Boyle and King', '7 Mifflin Lane', '2022-02-18', '4:45 AM', '0'),
(2, 12, 'Kuvalis, Harris and Hermann', 'Nulla ut erat id mauris vulputate elementum.', 'Ziemann LLC', '6221 Russell Road', '2022-03-27', '4:09 PM', '0'),
(3, 3, 'Crooks Inc', 'Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla.', 'Medhurst-Metz', '70 Spaight Hill', '2022-02-07', '12:34 AM', '0'),
(4, 9, 'Weissnat, Koss and Kuhn', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi.', 'Kautzer-Johnson', '4 Almo Place', '2022-02-10', '1:55 PM', '1'),
(5, 6, 'Ankunding-Wiegand', 'Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit.', 'Kirlin-Mohr', '5479 8th Alley', '2022-03-08', '1:51 PM', '1'),
(6, 6, 'Pouros and Sons', 'Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', 'Upton, Heathcote and Leffler', '966 Saint Paul Alley', '2022-02-27', '7:35 AM', '0'),
(7, 12, 'Quigley and Sons', 'Ut tellus.', 'Conroy-Roob', '21112 Starling Pass', '2022-03-16', '11:23 AM', '1'),
(8, 4, 'Stracke, Ziemann and Trantow', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 'Bradtke, Keebler and Lang', '457 Carberry Parkway', '2022-03-20', '9:41 PM', '0'),
(9, 11, 'Johnson, Lang and Lebsack', 'Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo.', 'Lowe-DuBuque', '13 Daystar Drive', '2022-03-12', '2:23 AM', '0'),
(10, 3, 'Olson-Cruickshank', 'Vestibulum rutrum rutrum neque.', 'Rempel and Sons', '5 Dakota Terrace', '2022-03-14', '11:04 AM', '1'),
(11, 4, 'Marquardt and Sons', 'Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo.', 'Roob-Adams', '2123 Dayton Center', '2022-02-25', '3:23 AM', '1'),
(12, 7, 'Balistreri and Sons', 'Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor.', 'Dare LLC', '39930 Jenifer Park', '2022-03-11', '10:56 AM', '0'),
(13, 8, 'Mante, Blick and Considine', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis. Donec semper sapien a libero.', 'Spinka-Bosco', '0817 Hoffman Road', '2022-03-19', '10:24 PM', '1'),
(14, 11, 'Jaskolski-Franecki', 'Nam dui.', 'Bosco, Lakin and Kuvalis', '2972 Surrey Plaza', '2022-03-21', '5:29 AM', '1'),
(15, 6, 'Hyatt Group', 'Phasellus in felis.', 'Aufderhar, Casper and Zboncak', '0 Banding Place', '2022-02-17', '4:06 AM', '0'),
(16, 9, 'Kilback, Bogisich and Wolff', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam.', 'Kassulke-Doyle', '6912 Welch Avenue', '2022-03-18', '3:41 AM', '0'),
(17, 2, 'Reichel, Towne and Langosh', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', 'Wintheiser, Schuppe and VonRueden', '11 Butternut Place', '2022-03-20', '3:42 AM', '1'),
(18, 3, 'Sanford Inc', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc.', 'Hoppe, Berge and Bergnaum', '59 Cambridge Court', '2022-03-14', '1:13 AM', '1'),
(19, 1, 'Smith, Ferry and Raynor', 'Pellentesque viverra pede ac diam.', 'Murphy and Sons', '28461 Judy Center', '2022-03-29', '5:57 PM', '0'),
(20, 9, 'Bechtelar, Lang and Howell', 'Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', 'O\'Keefe Group', '483 Valley Edge Circle', '2022-03-31', '3:28 AM', '0'),
(21, 10, 'Fay-Nitzsche', 'Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', 'Anderson-Lemke', '68 Dovetail Alley', '2022-03-10', '5:54 PM', '0'),
(22, 1, 'Gaylord-Predovic', 'Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla.', 'Fadel Inc', '61432 Arrowood Center', '2022-01-22', '3:18 AM', '1'),
(23, 3, 'Kemmer, Hamill and Johnson', 'Pellentesque ultrices mattis odio. Donec vitae nisi.', 'Brakus-Pacocha', '28636 Little Fleur Alley', '2022-03-09', '2:26 PM', '0'),
(24, 4, 'O\'Kon-Goldner', 'Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo.', 'Goldner, Gorczany and Green', '7775 Dayton Trail', '2022-03-16', '2:17 PM', '0'),
(25, 12, 'Becker, Baumbach and Schinner', 'Maecenas pulvinar lobortis est. Phasellus sit amet erat.', 'Jacobi, Howell and Howe', '1789 Sachtjen Junction', '2022-03-30', '4:25 PM', '1'),
(26, 4, 'Legros-Fadel', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy.', 'Ryan LLC', '8 Cherokee Avenue', '2022-03-26', '10:27 AM', '0'),
(27, 3, 'Hermiston LLC', 'In est risus, auctor sed, tristique in, tempus sit amet, sem.', 'Lynch Inc', '26259 Hanover Parkway', '2022-04-19', '6:36 AM', '1'),
(28, 6, 'Keeling and Sons', 'Nulla ut erat id mauris vulputate elementum. Nullam varius.', 'Jones-Douglas', '03 Declaration Road', '2022-03-07', '5:47 PM', '0'),
(29, 1, 'O\'Reilly LLC', 'Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo.', 'Kirlin-Muller', '82 Moland Alley', '2022-02-15', '7:20 PM', '1'),
(30, 1, 'Bergstrom Group', 'Phasellus in felis. Donec semper sapien a libero.', 'Heathcote-Muller', '15 Morrow Trail', '2022-04-20', '3:18 AM', '1'),
(31, 12, 'O\'Kon LLC', 'Praesent lectus. Vestibulum quam sapien, varius ut, blandit non, interdum in, ante.', 'Rosenbaum-Effertz', '333 Algoma Park', '2022-04-27', '11:10 PM', '1'),
(32, 7, 'Murray, Schiller and Jenkins', 'Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 'Emard-Sauer', '43771 American Lane', '2022-02-23', '4:48 PM', '0'),
(33, 9, 'Gaylord Inc', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue.', 'Reichert-Hintz', '97870 Dwight Park', '2022-04-01', '6:02 PM', '0'),
(34, 4, 'Stiedemann LLC', 'Proin risus. Praesent lectus. Vestibulum quam sapien, varius ut, blandit non, interdum in, ante.', 'Pacocha LLC', '960 South Street', '2022-04-27', '9:00 AM', '0'),
(35, 6, 'Schinner Inc', 'Nulla ac enim.', 'Doyle-Walker', '662 Morning Crossing', '2022-04-22', '1:08 AM', '1'),
(36, 2, 'Walter-Haley', 'Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam. Nam tristique tortor eu pede.', 'Langosh, Prosacco and Abernathy', '864 Norway Maple Trail', '2022-04-26', '2:19 AM', '0'),
(37, 5, 'Fisher-Thompson', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo.', 'Torphy-Keebler', '764 Bunker Hill Park', '2022-02-26', '11:55 PM', '0'),
(38, 11, 'Dach-Bosco', 'In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue.', 'Thiel-Bruen', '216 Carey Park', '2022-04-18', '7:16 AM', '1'),
(39, 1, 'O\'Keefe-DuBuque', 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum.', 'Goodwin, Hoppe and Friesen', '10141 Upham Lane', '2022-04-21', '8:30 AM', '1'),
(40, 9, 'Halvorson-Abernathy', 'Vivamus in felis eu sapien cursus vestibulum. Proin eu mi.', 'Sporer-Wiegand', '037 Coolidge Circle', '2022-03-22', '7:40 PM', '0');

-- --------------------------------------------------------

--
-- Table structure for table `organiser`
--

CREATE TABLE `organiser` (
  `organiserID` int(11) NOT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `organiser`
--

INSERT INTO `organiser` (`organiserID`, `firstname`, `lastname`, `email`) VALUES
(1, 'Beauregard', 'Challiss', 'bchalliss0@vkontakte.ru'),
(2, 'Karla', 'Woollends', 'kwoollends1@live.com'),
(3, 'Dorice', 'Tattoo', 'dtattoo2@ted.com'),
(4, 'Christyna', 'Larratt', 'clarratt3@aboutads.info'),
(5, 'Gusta', 'Dearlove', 'gdearlove4@fastcompany.com'),
(6, 'Stoddard', 'Ughetti', 'sughetti5@ning.com'),
(7, 'Kelvin', 'Curtin', 'kcurtin6@wired.com'),
(8, 'Tiphani', 'Bracchi', 'tbracchi7@accuweather.com'),
(9, 'Odell', 'Alen', 'oalen8@behance.net'),
(10, 'Dietrich', 'Lowseley', 'dlowseley9@php.net'),
(11, 'Risa', 'Recher', 'rrechera@diigo.com'),
(12, 'Tobe', 'Chown', 'tchownb@cbslocal.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`adminID`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`eventID`),
  ADD KEY `organiserID` (`organiserID`);

--
-- Indexes for table `organiser`
--
ALTER TABLE `organiser`
  ADD PRIMARY KEY (`organiserID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `adminID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `eventID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `organiser`
--
ALTER TABLE `organiser`
  MODIFY `organiserID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `event`
--
ALTER TABLE `event`
  ADD CONSTRAINT `event_ibfk_1` FOREIGN KEY (`organiserID`) REFERENCES `organiser` (`organiserID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
