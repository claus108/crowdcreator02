<?php
    // controller is called via AJAX, and is not running via index.php
    // database file has to be included
    include_once('../model/database.php');

    // call database function and return values as json object
    $event_dates = get_event_dates();
    echo json_encode($event_dates);
?>