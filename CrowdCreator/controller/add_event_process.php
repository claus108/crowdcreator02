<?php
// retrieve all form data
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $firstname = sanitize_input($_POST['fname']);
    $lastname = sanitize_input($_POST['lname']);
    $email = sanitize_input($_POST['email']);
    $title = sanitize_input($_POST['event-title']);
    $description = sanitize_input($_POST['description']);
    $venue = sanitize_input($_POST['venue']);
    $address = sanitize_input($_POST['address']);
    $date = sanitize_input($_POST['date']);
    $time = sanitize_input($_POST['time']);
}

    // input data sanitization
    function sanitize_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

      // data validation
      validateData($firstname, $lastname, $email, $title, $description, $address, $date, $time);

      function validateData($firstname,$lastname, $email, $title, $description, $address, $date, $time){
        // fields required
        if(empty($firstname) || empty($lastname) || empty($email) || empty($description)  || empty($address) || empty($date) || empty($time)) {
          header('location:index.php?p=input_error&error_type=required');
        }
        // email valid
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
          header('location:index.php?p=input_error&error_type=email');
        }
      }
    
    // call database functions for adding data to respective database
    $organiserID = add_organiser($firstname, $lastname, $email);
    $result = add_event($organiserID, $title, $description, $venue, $address, $date, $time);

?>