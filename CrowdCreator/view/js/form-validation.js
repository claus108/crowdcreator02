$(function() {
    // get all form elements by their id
    const fname = document.getElementById('fname');
    const lname = document.getElementById('lname');
    const email = document.getElementById('email');
    const title = document.getElementById('event-title');
    const description = document.getElementById('description');
    const address = document.getElementById('address');
    const date = document.getElementById('date');
    const time = document.getElementById('time');

    // prevent form default behaviour unless all validation requirements are met
    document.getElementById('create-event-form').addEventListener('submit', e => {
        if (!validateInputs()) {
            e.preventDefault();
        }
    });

    function validateInputs() {
        // trim to remove the whitespaces
        const fnameValue = fname.value.trim();
        const lnameValue = lname.value.trim();
        const emailValue = email.value.trim();
        const titleValue = title.value.trim();
        const descriptionValue = description.value.trim();
        const addressValue = address.value.trim();
        const dateValue = date.value.trim();
        const timeValue = time.value.trim();

        var isValid = true;
        //check if first name is empty
        if (fnameValue === "") {
            errorMessage(fname, "Your first name is required.");
            isValid = false;
        } else {
            setSuccess(fname);
        }

        if (lnameValue === "") {
            errorMessage(lname, 'Your last name is required.');
            isValid = false;
        } else {
            setSuccess(lname);
        }

        if (emailValue === "") {
            errorMessage(email, 'Email is required');
            isValid = false;
        } else if (!isEmail(emailValue)) {
            errorMessage(email, "Please enter a valid email");
            isValid = false;
        } else {
            setSuccess(email);
        }

        if (titleValue === "") {
            errorMessage(title, "An event title is required.");
            isValid = false;
        } else {
            setSuccess(title);
        }
        if (descriptionValue === "") {
            errorMessage(description, "An event description is required.");
            isValid = false;
        } else {
            setSuccess(description);
        }

        if (addressValue === "") {
            errorMessage(address, "A venue address is required.");
            isValid = false;
        } else {
            setSuccess(address);
        }
        if (dateValue === "") {
            errorMessage(date, "An event date is required.");
            isValid = false;
        } else {
            setSuccess(date);
        }
        if (timeValue === "") {
            errorMessage(time, "An event time is required.");
            isValid = false;
        } else {
            setSuccess(time);
        }

        return isValid;
    }
    // validate the email with a regular expression 
    function isEmail(email) {
        return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
    }
    // display error message to the respective field
    function errorMessage(input, message) {
        const formControl = input.parentElement;
        const small = formControl.querySelector('small');
        formControl.className = 'form-group error';S
        small.innerText = message;
    }
    // remove potential error messages
    function setSuccess(input) {
        const formControl = input.parentElement;
        const small = formControl.querySelector('small');
        formControl.className = '';
        small.style.visibility = "hidden";
    }

});