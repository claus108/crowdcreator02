$(function() {
    // navbar
    $('.navbar-toggler').click(function() {
        $('#navbarNav').toggleClass('collapse');
    });

    // calendar 
    $('.myCalendar').huicalendar({
    })

    // Create Event Form
    $('#details-form-box').show();
    $('#event-form-box').hide();

    $('#register-next').click(function() {
        $('#details-form-box').hide();
        $('#event-form-box').show();
    })

    $('#event-back').click(function() {
        $('#details-form-box').show();
        $('#event-form-box').hide();
    })

    // Events page
    const d = new Date();
    const month = d.getMonth() + 1;

    $('#current-month-link').addClass('active');
    $('.first-month-after'). hide();
    $('.second-month-after').hide();

    $('#current-month-link').click(function() {
        $('.current-month').show();
        $('.first-month-after'). hide();
        $('.second-month-after').hide();
        $('#current-month-link').addClass('active');
        $('#first-month-after-link').removeClass('active');
        $('#second-month-after-link').removeClass('active');
    });
    $('#first-month-after-link').click(function() {
        $('.current-month').hide();
        $('.first-month-after'). show();
        $('.second-month-after').hide();
        $('#current-month-link').removeClass('active');
        $('#first-month-after-link').addClass('active');
        $('#second-month-after-link').removeClass('active');

    });
    $('#second-month-after-link').click(function() {
        $('.current-month').hide();
        $('.first-month-after'). hide();
        $('.second-month-after').show();
        $('#current-month-link').removeClass('active');
        $('#first-month-after-link').removeClass('active');
        $('#second-month-after-link').addClass('active');
    });

});

