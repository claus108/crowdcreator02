
<div class="form-container">
    <!-- display messages in case of server-side errors -->
    <?php
        if($error_type == 'required'){
            echo "<div class='error-box'>";
            echo "<i>*</i> fields are required";
            echo "</div>";
        }
        if($error_type == 'email'){
            echo "<br><div class='error-box'>";
            echo "Please enter a valid email";
            echo "</div>";
        }
    ?>
    
    <form id="create-event-form" action="index.php" method="POST">
        <input type='hidden' name='p' value='add_event_process'>
        <div id='details-form-box' class='panel'><!-- start details-form-box -->
            <h2>Your Details</h2>
            <br><small><i class="required">*</i> fields are required</small><br><br>
            <div class='form-group'>
                <label for='fname'>First Name <i class="required">*</i></label>
                <input type='text' class='form-control' id='fname' name='fname' value='Test'>
                <small></small>
                <br>
            </div>
            <div class='form-group'>
                <label for='lname'>Last Name <i class="required">*</i></label>
                <input type='text' class='form-control' id='lname' name='lname'  value='Last'>
                <small></small>
                <br>
            </div>
            <div class='form-group'>
                <label for='email'>Email <i class="required">*</i></label>
                <input type="text" class='form-control' id='email' name='email' value='test@test.au'>
                <small></small>
                <br>
            </div>
            <br><br>
            <button id="register-next" class="next btn btn-light" type='button' value='Next'> 
                NEXT <i class="fa fa-angle-double-right" aria-hidden="true"></i>
            </button>

        </div><!-- End details-form-box -->

        <div id='event-form-box' class='panel'><!-- start event-form-box -->
            <h2>Your Event</h2>
            <div class="form-group">
                <label for='event-title'>Title <i class="required">*</i></label><br>
                <input type='text' class="form-control" id='event-title' name='event-title' value='Title'>
                <small></small>
            </div>
            <div class="form-group">
                <label for='description'>Description <i class="required">*</i></label><br>
                <textarea class="form-control" id='description' name='description' rows='3'>Test description</textarea>
                <small></small>
            </div>
            <div class="form-group">
                <label for='venue'>Venue </label><br>
                <input class="form-control" type='text' id='venue' name='venue' value='Test Venue'><br>
            </div>
            <div class="form-group">
                <label for='address'>Address <i class="required">*</i></label><br>
                <input class="form-control" type='text' id='address' name='address' value='102 Test Street' placeholder='100 Some Street'>
                <small></small>
            </div>
            <div class="form-group">
                <label for='date'>Date <i class="required">*</i></label><br>
                <input class="form-control" type='date' id='date' name='date' value='2022-02-21'>
                <small></small>
            </div>
            <div class="form-group">
                <label for='time'>Time <i class="required">*</i></label><br>
                <input class="form-control" type='text' id='time' name='time' value='8PM'>
                <small></small>
            </div>
            <br><br>
            <button id="event-back" class="back btn btn-light" type='button' value='Previous'> 
                <i class="fa fa-angle-double-left" aria-hidden="true"></i> PREVIOUS
            </button>
            <input class="next btn btn-primary" type='submit' value='Submit Your Event'>
        </div><!-- End of event-form-box -->
    </form>
</div><!-- End of form container -->

<script src="view/js/form-validation.js"></script>

