<h2>Events</h2>
<br>
<ul class="nav nav-tabs">
    <li class="nav-item">
      <a id="current-month-link" class="nav-link" href="#"><?= date("F") ?></a>
    </li>
    <li class="nav-item">
      <a id="first-month-after-link" class="nav-link" href="#"><?= date('F',strtotime('first day of +1 month')); ?></a>
    </li>
    <li class="nav-item">
      <a id="second-month-after-link" class="nav-link" href="#"><?= date('F',strtotime('first day of +2 month')); ?></a>
    </li>
  </ul>
<?php
    foreach($events as $event) {
      $month = date('m');
      $current_month = intval($month);
      $first_month = $current_month + 1;
      $second_month = $first_month + 1;

      switch(substr($event['date'], 5, 2)) {
          case $first_month:
            echo "<div class='box first-month-after'>";
            include './view/event.php';
            echo "</div>";
            break;
          case $second_month:
            echo "<div class='box second-month-after'>";
            include './view/event.php';
            echo "</div>";
            break;
          default :
            echo "<div class='box current-month'>";
            include './view/event.php';
            echo "</div>";
            break;
      }
    }
?>