            </div>
            <div class="col-sm-2">
                <h2>Pick a Day</h2>
                <br>
                <div class="myCalendar">
                </div>
            </div>
        </div> <!-- END of row -->
    </div> <!-- END of container-fluid -->
<footer>
    <div class="footer-nav">
        <a href="index.php">Home</a>
        <a href="index.php?p=events">Events</a>
        <a href="index.php?p=add_event">Create Event</a>
        <a href="index.php?p=about">About Us</a>
    </div>
    <div id="copyright">
    &copy; Claus Hennig  
        <?php 
            // use the date(format, timestamp) function for displaying the current year
            // with format 'Y' representing a four digit year.
            echo date('Y'); 
        ?>
    </div>
</footer>
    <script src="view/js/scripts.js"></script>
  
</body>

</html>