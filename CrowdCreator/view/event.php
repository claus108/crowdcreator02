<div class="event-box"> 
    <div class="date-box">
        <p><?= $event['date'] ?></p>
        <p><?= $event['time'] ?></p>
    </div>
    <div class="venue-box">
        <h3><?= $event['title'] ?></h3>
        <p class="venue">venue: <?= $event['venue'] ?></p>
        <p>address: <?= $event['address'] ?></p>
        <p><?= $event['description'] ?></p>
    </div>
</div>