
<div>
    <?php 
        if($result == '1') {
            echo "<div class='response-box response-success'>";
            echo "<h2>Thank you for submitting your event</h2>";
            echo "</div>";
        } else {
            echo "<div class='response-box bg-warning'>";
            echo "<h2>An error occured during your booking process</h2>
            <p>Please try <a href='index.php?p=add_event'>again</a></p>";
            echo "</div>";
        }
    ?>
</div>