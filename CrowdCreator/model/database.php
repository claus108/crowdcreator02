<?php 

    function getPDO(){
        $host = "localhost";
        $user = "root";
        $pass = "";
        $db = "ch_crowdcreator";
        
        $options = [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION];
        $dsn =  "mysql:host=$host;dbname=$db;ssl-mode=required";
        try {
            $pdo = new PDO($dsn,$user,$pass);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $pdo;
        } catch(PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }
    
    $pdo = getPDO();

    // retrieve the next 5 upcoming events sorted by date and time
    function get_upcoming_events() {
        global $pdo;
        $sql = "SELECT * FROM event WHERE approved = 1 ORDER BY date, time  LIMIT 4";
        $stmnt = $pdo -> prepare($sql);
        $stmnt->execute();
        $upcoming_events = $stmnt->fetchAll(PDO::FETCH_ASSOC);
        return $upcoming_events;
    }

    // retrieve all approved events
    function get_events() {
        global $pdo;
        $sql = "SELECT * FROM event WHERE approved = 1 ORDER BY date, time";
        $stmnt = $pdo->prepare($sql);
        $stmnt->execute();
        $events = $stmnt->fetchAll(PDO::FETCH_ASSOC);

        return $events;
    }

    // retrieve approved events according to date selected in the calendar
    function get_events_of_date($date) {
        global $pdo;
        $sql = "SELECT * FROM event WHERE approved = 1 AND date = :date ORDER BY date, time";
        $stmnt = $pdo->prepare($sql);
        $stmnt->bindValue(':date', $date);
        $stmnt->execute();
        $events = $stmnt->fetchAll(PDO::FETCH_ASSOC);

        return $events;
    } 

    // retrieve all approved event dates for highlighting days in calendar that have upcoming events
    function get_event_dates() {
        global $pdo;
        $sql = "SELECT date FROM event WHERE approved = 1";
        $stmnt = $pdo->prepare($sql);
        $stmnt->execute();
        $event_dates = $stmnt->fetchAll(PDO::FETCH_ASSOC);

        return $event_dates;
    }

    // add event organiser to the organiser database table
    function add_organiser($firstName, $lastName, $email) {
        global $pdo;
        $sql = "INSERT INTO organiser (firstname, lastname, email) VALUES (:firstname, :lastname, :email)";
        $statement = $pdo->prepare($sql);
        $statement->bindValue(':firstname', $firstName);
        $statement->bindValue(':lastname', $lastName);
        $statement->bindValue(':email', $email);
        $result = $statement->execute();
        $id = $pdo->lastInsertId();
        
        return $id;
    }

    // add created event from the form to the event database table
    function add_event($organiserID, $title, $description, $venue, $address, $date, $time) {
        global $pdo;
        $sql = "INSERT INTO event (organiserID, title, description, venue, address, date, time, approved) VALUES (:organiserID, :title, :description, :venue, :address, :date, :time, :approved)";
        $statement = $pdo->prepare($sql);
        $statement->bindValue(':organiserID', $organiserID);
        $statement->bindValue(':title', $title);
        $statement->bindValue(':description', $description);
        $statement->bindValue(':venue', $venue);
        $statement->bindValue(':address', $address);
        $statement->bindValue(':date', $date);
        $statement->bindValue(':time', $time);
        $statement->bindValue(':approved', 0);
        $result = $statement->execute();
        
        return $result;
    }
?>