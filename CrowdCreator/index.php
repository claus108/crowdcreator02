<?php
    // import database file 
    include 'model/database.php';

    //Validate which page to show
    if(isset($_GET['p'])) {
        $p = $_GET['p'];
    } elseif (isset($_POST['p'])) {
        $p = $_POST['p'];
    } else {
        $p = NULL;
    }

    $error_type =  $result = '';

    //Determine, which page page to show
    switch ($p) {
        case 'about':
            $page = 'about.php';
            $page_title = 'About | CrowdCreator';
            break;
        case 'input_error':
            if(isset($_GET['error_type'])){
                $error_type = $_GET['error_type'];
            }
        case 'add_event':
            $page = 'event_submit_form.php';
            $page_title = 'Your Event | CrowdCreator';
            break;
        case 'add_event_process' :
            include 'controller/add_event_process.php';
            $page = 'added_event.php';
            $page_title = "Event Submitted | CrowdCreator";
            break;
        case 'events_of_date':
            include 'controller/get_events_of_date.php';
            $page = 'events_of_date.php';
            $page_title = 'Events | CrowdCreator';
            break;
        case 'events':
            include 'controller/get_events_process.php';
            $page = 'events.php';
            $page_title = 'Events | CrowdCreator';
            break;
        default:
            include 'controller/get_upcoming_events_process.php';
            $page = 'main.php';
            $page_title = 'Home | CrowdCreator';;
    }

    // make sure the choosen file actually exists
    if(!file_exists('./view/' . $page)) {
        $page = 'main.php';
        $page_title = "Home | Bollywood";
    }

    // include the header
    include ('./view/header.php');

    // include the content according to switch statement outcome.
    include('./view/' . $page);

    // include the footer
    include ('./view/footer.php');
?>

