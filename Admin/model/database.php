<?php 

    function getPDO(){
        $host = "localhost";
        $user = "root";
        $pass = "";
        $db = "ch_crowdcreator";
        
        $options = [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION];
        $dsn =  "mysql:host=$host;dbname=$db;ssl-mode=required";
        try {
            $pdo = new PDO($dsn,$user,$pass);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $pdo;
        } catch(PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }
    
    $pdo = getPDO();
    
    // this function can be used to add a new admin staff, but is currently not in use
    // function add_admin($username, $password) {
    //     global $pdo;
    //     $sql = "INSERT INTO admin (username, password) VALUES (:uname, :password)";
    //     $statement = $pdo->prepare($sql);
    //     $statement->bindValue(':uname', $username);
    //     $statement->bindValue(':password', $password);
    //     $result = $statement->execute();
    //     return $result;  
    // }

    // retrieve the password from the database to compare with the login form password
    function get_password($username) {
        global $pdo;
        $sql = "SELECT password FROM admin WHERE username = :username";
        $stmnt = $pdo->prepare($sql);
        $stmnt->bindParam(':username', $username);
        $stmnt->execute();
        $password = $stmnt->fetch(PDO::FETCH_ASSOC);
        return $password;
    }

    // get all events form the database and order them by date and time
    function get_events() {
        global $pdo;
        $sql = "SELECT * FROM event ORDER BY date, time";
        $stmnt = $pdo->prepare($sql);
        $stmnt->execute();
        $events = $stmnt->fetchAll(PDO::FETCH_ASSOC);

        return $events;
    }

    // get one event 
    function get_event($eventID) {
        global $pdo;
        $sql = "SELECT * FROM event WHERE eventID = :eventID";
        $stmnt = $pdo->prepare($sql);
        $stmnt->bindParam(':eventID', $eventID);
        $stmnt->execute();
        $event = $stmnt->fetch();

        return $event;
    }

    // delete one event
    function delete_event($eventID) {
            global $pdo;
            $sql = "DELETE FROM event WHERE eventID = :eventID";
            $statement = $pdo->prepare($sql);
            $statement->bindValue(':eventID', $eventID);
            $result = $statement->execute();

            return $result;
    }

    // approve or deny event according to its status
    function approve_event($event_status, $eventID) {
        global $pdo;
        $sql = "UPDATE event SET approved = :event_status WHERE eventID = :eventID";
        $statement = $pdo->prepare($sql);
        $statement->bindValue(':event_status', $event_status);
        $statement->bindValue(':eventID', $eventID);
        $result = $statement->execute();

        return $result;
    }

    // add a new event to the database
    function update_event($eventID, $title, $description, $venue, $address, $date, $time) {
        global $pdo;
        $sql = "UPDATE event
            SET title = :title,
                description = :description,
                venue = :venue,
                address = :address,
                date = :date,
                time = :time
            WHERE eventID = :eventID";
        $statement = $pdo->prepare($sql);
        $statement->bindValue(':eventID', $eventID);
        $statement->bindValue(':title', $title);
        $statement->bindValue(':description', $description);
        $statement->bindValue(':venue', $venue);
        $statement->bindValue(':address', $address);
        $statement->bindValue(':date', $date);
        $statement->bindValue(':time', $time);
        $result = $statement->execute();

        return $result;
    }

    // retrieve all organisers 
    function get_organiser() {
        global $pdo;
        $sql = "SELECT * FROM organiser";
        $stmnt = $pdo->prepare($sql);
        $stmnt->execute();
        $organiser = $stmnt->fetchAll(PDO::FETCH_ASSOC);

        return $organiser;
    }



?>