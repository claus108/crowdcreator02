
<h2>Events</h2>
    <table class="admin-table">
        <tr>
            <th>approve</th>
            <th>edit</th>
            <th>delete</th>
            <th>id</th>
            <th>approved</th>
            <th>organiser</th>
            <th>title</th>
            <th>venue</th>
            <th>date</th>
            <th>time</th>
        </tr>
    <?php
        $i = 0;
        $color1 = '#f2f2f2';
        $color2 = '#ffffff';
        // dynamically create table after retrieving all events
        foreach($events as $event) {
            echo "<tr style='background-color: " . ( ($i % 2 == 0) ? $color1 : $color2) . "'>";
            if($event['approved'] == 0) {
                echo "<td><a class='btn btn-success btn-sm' href='index.php?p=approve&status=1&id=" . $event['eventID'] . "'>approve</a></td>";
            } else {
                echo "<td><a class='btn btn-warning btn-sm' href='index.php?p=approve&status=0&id=" . $event['eventID'] . "'>deny</a></td>";
            }
            echo "<td><a href='index.php?p=event&id=" . $event['eventID'] . "' class='btn btn-primary btn-sm'>update</a></td>";
            echo "<td><a class='btn btn-danger btn-sm' href='index.php?p=delete&id=" . $event['eventID'] . "'>delete</a></td> ";
            echo "<td>" . $event['eventID'] . "</td>";
            echo "<td>" . $event['approved'] . "</td>";
            echo "<td>" . $event['organiserID'] . "</td>";
            echo "<td>" . $event['title'] . "</td>";
            echo "<td>" . $event['venue']. "</td>";
            echo "<td>" . $event['date'] . "</td>";
            echo "<td>" . $event['time'] . "</td>";
            echo "<tr>";
            $i++;
        }
    ?>
    </table>


