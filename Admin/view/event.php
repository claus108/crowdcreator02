
<div class="form-box">
    <form action="index.php" method="POST">
        <input type='hidden' name='p' value='update'>
        <input type='hidden' name='eventID' value='<?= $event['eventID'] ?>'>
        <div class="form-group">
            <label for='title'>Title</label><br>
            <input type='text' class="form-control" id='title' name='title' value='<?= $event['title'] ?>'>
            <small></small>
        </div>
        <div class="form-group">
            <label for='description'>Description</i></label><br>
            <textarea class="form-control" id='description' name='description' rows='3'><?= $event['description'] ?></textarea>
            <small></small>
        </div>
        <div class="form-group">
            <label for='venue'>Venue </label><br>
            <input class="form-control" type='text' id='venue' name='venue' value='<?= $event['venue'] ?>'><br>
        </div>
        <div class="form-group">
            <label for='address'>Address</label><br>
            <input class="form-control" type='text' id='address' name='address' value='<?= $event['address'] ?>' placeholder='100 Some Street'>
            <small></small>
        </div>
        <div class="form-group">
            <label for='date'>Date</label><br>
            <input class="form-control" type='text' id='date' name='date' value='<?= $event['date'] ?>'>
            <small></small>
        </div>
        <div class="form-group">
            <label for='time'>Time <i class="required">*</i></label><br>
            <input class="form-control" type='text' id='time' name='time' value='<?= $event['time'] ?>'>
            <small></small>
        </div>
        <br>
        <div>Approved: <?= $event['approved'] ?><div>
        <br><br>
        <a class='btn btn-danger btn' href='index.php?p=delete&id='<?= $event['eventID'] ?>'>DELETE</a>
        <?php
            if($event['approved'] == '0') {
                echo "<a class='btn btn-success btn' href='index.php?p=approve&status=1&id=" . $event['eventID'] . "'>APPROVE</a>";
            } else {
                echo "<a class='btn btn-warning btn' href='index.php?p=approve&status=0&id=" . $event['eventID'] . "'>DENY</a>";
            }
        ?>
        <input class="btn btn-primary" type='submit' value='Update'>
        <php

    </form>


</div>