<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    <!-- <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script> -->
    <script src="view/js/jquery/jquery.js"></script>
    <link rel="stylesheet" href="view/css/styles.css">
    <!-- hui calendar -->
    <link rel="stylesheet" href="view/css/huicalendar.css" />
    <script src="view/js/huicalendar.js"></script>
    <title><?php echo $page_title; ?></title>
</head>

<body>
    <header>
        <div id="title-box">
            <h1 id="title">CrowdCreator Admin <i class='fa-solid fa-trash-can'></i></h1>
        </div>
        <?php
        if(isset($_SESSION['id'])) {
            echo <<<END
            <nav class='navbar navbar-expand-lg navbar-light bg-light'>
                <a class='navbar-brand' href="#">CrowdCreator</a>
                <button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarNav' aria-controls='navbarNav' aria-expanded='false' aria-label='Toggle navigation'>
                    <span class='navbar-toggler-icon'></span>
                </button>
                <div class='collapse navbar-collapse' id='navbarNav'>
                    <ul class='navbar-nav'>
                        <li class='nav-item'>
                            <a class='nav-link' href='index.php?p=events'>Events</a>
                        </li>
                        <li class='nav-item'>
                            <a class='nav-link' href='index.php?p=organiser'>Organiser</a>
                        </li>
                        <li class='nav-item'>
                            <a class='nav-link ' href='index.php?p=logout'>Log out</a>
                        </li>
                    </ul>
                </div>
            </nav>
            END;
        }
        ?>

    </header>
    <div class="content">
    <div class="container">