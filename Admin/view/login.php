<div class="form-container">
    
    <form action="index.php" method="POST">
        <input type='hidden' name='p' value='login_process'>
        <!-- <input type='hidden' name='p' value='signup_process'> -->
        <?php
            if($login_error){
                echo "<div class='error-box'>";
                echo "Incorrect username or password";
                echo "</div>";
            }
        ?>
        <div id='details-form-box' class='panel'>
            <h2>LOG IN</h2>
            <div class='form-group'>
                <label for='username'>Username</label>
                <input type='text' class='form-control' id='username' name='username' value='clhen106'>
            </div>
            <div class='form-group'>
                <label for='password'>Password</label>
                <input type='password' class='form-control' id='password' name='password' value='password'>
            </div>
            <br><br>
            <input class="btn btn-primary" type='submit' value='LOG IN'>
        </div>
    </form>
</div><!-- End of form container -->

<script src="view/js/form-validation.js"></script>

