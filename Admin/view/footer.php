
        </div> <!-- END of container -->
    </div> <!-- END of content -->
<footer>
<?php
    if(isset($_SESSION['id'])) {
        echo <<<END
            <div class="footer-nav">
                <a href="index.php">Home</a>
                <a href='index.php?p=events'>Events</a>
                <a href="index.php?p=register">Organiser</a>
                <a href='index.php?p=logout'>Log out</a>
            </div>
            END;
        }
?>                
    <div id="copyright">
    &copy; Claus Hennig  
        <?php 
            // use the date(format, timestamp) function for displaying the current year
            // with format 'Y' representing a four digit year.
            echo date('Y'); 
        ?>
    </div>
</footer>
    <script src="view/js/scripts.js"></script>
</body>

</html>