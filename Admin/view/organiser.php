
<h2>Organiser</h2>
    <table class="admin-table">
        <tr>
            <th>id</th>
            <th>first name</th>
            <th>last name</th>
            <th>email</th>
        </tr>
    <?php
        $i = 0;
        $color1 = '#f2f2f2';
        $color2 = '#ffffff';
        // dynamically create table data after retrieving all organisers
        foreach($organiser as $org) {
            echo "<tr style='background-color: " . ( ($i % 2 == 0) ? $color1 : $color2) . "'>";
                echo "<td>" . $org['organiserID'] . "</td>";
                echo "<td>" . $org['firstname'] . "</td>";
                echo "<td>" . $org['lastname']. "</td>";
                echo "<td>" . $org['email'] . "</td>";
            echo "<tr>";
            $i++;
        }
    ?>
    </table>


