<?php
    // retrieve data from login form
    $username = $_POST['username'];
    $password = $_POST['password'];

    // retrieve the password stored in the database
    $db_password = get_password($username);

    // redirect back if no password is associated with the username
    if(!$db_password) {
        header('location:index.php?p=login_error');
    }

    // verify the password and start a new session
    if(password_verify($password, $db_password['password'])) {
        if(!isset($_SESSION)) {
            session_start();
        }
        // create a random session id
        $session_id = session_id();
        $_SESSION['id'] = $session_id;
        $_SESSION['username'] = $username;
    } else {
        header('location:index.php?p=login_error');
    }

?>