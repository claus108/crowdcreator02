<?php

    if(!isset($_SESSION)) {
        session_start();
    }

    // import database file 
    include 'model/database.php';

    //Validate which page to show
    if(isset($_GET['p'])) {
        $p = $_GET['p'];
    } elseif (isset($_POST['p'])) {
        $p = $_POST['p'];
    } else {
        $p = NULL;
    }

    $login_error = false;
    $page = $page_title = "";

    //Determine, which page to show
    switch ($p) {
        case 'delete' :
            if(isset($_SESSION['id'])){
                include 'controller/delete_event_process.php';
                include 'controller/get_events_process.php';
                $page = 'events.php';
                $page_title = "Events | CrowdCreator";
            }
            break;
        case 'approve' :
            if(isset($_SESSION['id'])){
                include 'controller/approve_event_process.php';
                include 'controller/get_events_process.php';
                $page = 'events.php';
                $page_title = "Events | CrowdCreator";
            }
            break;
        case 'update' :
            if(isset($_SESSION['id'])){
                include 'controller/update_event_process.php';
                include 'controller/get_events_process.php';
                $page = 'events.php';
                $page_title = "Events | CrowdCreator";
            }
            break;
        case 'event' :
            if(isset($_SESSION['id'])){
                include 'controller/get_event_process.php';
                $page = 'event.php';
                $page_title = "Event | CrowdCreator";
            }
            break;
        case 'events':
            if(isset($_SESSION['id'])){
                include 'controller/get_events_process.php';
                $page = 'events.php';
                $page_title = 'Events | CrowdCreator';
            }
            break;
        case 'organiser' :
            if(isset($_SESSION['id'])){
                include 'controller/get_organiser_process.php';
                $page = 'organiser.php';
                $page_title = 'Organiser | CrowdCreator';
            }
            break;
        // case 'signup_process':
        //     include 'controller/signup_process.php';
        //     break;
        case 'logout' :
            if(isset($_SESSION['id'])){
                include 'controller/logout_process.php';
                $page = 'login.php';
                $page_title = 'Login | CrowdCreator';
            }
            break;
        case 'login_process':
            include 'controller/login_process.php';
            if(isset($_SESSION['id'])){
                include 'controller/get_events_process.php';
                $page = 'events.php';
                $page_title = 'Events | CrowdCreator';
            }
            break;
        case 'login_error':
            $login_error = true;
            $page = 'login.php';
            $page_title = 'Login | CrowdCreator'; 
            break;
        default:
            $page = 'login.php';
            $page_title = 'Login | CrowdCreator';
    }

    // make sure the choosen file actually exists
    if(!file_exists('./view/' . $page)) {
        $page = 'main.php';
        $page_title = "Home | Bollywood";
    }

    // include the header
    include ('./view/header.php');

    // include the content according to switch statement outcome.
    include('./view/' . $page);

    // include the footer
    include ('./view/footer.php');


    


    // https://www.jqueryscript.net/time-clock/powerful-calendar.html
    // https://www.jqueryscript.net/time-clock/hui-month-calendar.html
?>

